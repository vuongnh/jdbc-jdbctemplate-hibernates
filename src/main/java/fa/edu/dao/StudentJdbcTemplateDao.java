package fa.edu.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import fa.edu.model.Student;

@Component
public class StudentJdbcTemplateDao {
	
	@Autowired
	private DataSource dataSource;
	
	private JdbcTemplate jdbcTemplate;
	
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void insertStudent(Student student) {
		
			String sql= "INSERT INTO student VALUES ("
					 + student.getId() + ",'" + student.getName() + "','" + student.getLocation() + "')";
			jdbcTemplate.execute(sql);
			
			System.out.println("data inserted !!");
			
	}
	public void delelteStudent() {
		
		String sql= "DELETE FROM STUDENT";
		jdbcTemplate.execute(sql);
		
		System.out.println("data deleted !!");
		
	}
	public int count() {
		String query = "Select count(*) from student";
		return jdbcTemplate.queryForObject(query, Integer.class);
	}
	
	public Student getStudentById(int id) {
		String query = "select * from student where id = ?";
		return jdbcTemplate.queryForObject(query, new Object[] {id}, new StudentMapper());
	}
	
	public List<Student> getAllStudent() {
		String query = "SELECT * FROM STUDENT";
		return jdbcTemplate.query(query, new StudentMapper());
	}
	
	
	private static final class StudentMapper implements RowMapper<Student> {
		
		public Student mapRow(ResultSet rs, int arg1) throws SQLException{
			return new Student(rs.getInt("id"),rs.getString("name"),rs.getString("location"));
		}
	}
	
}

