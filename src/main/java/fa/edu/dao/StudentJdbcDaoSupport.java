package fa.edu.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import fa.edu.model.Student;

@Component
public class StudentJdbcDaoSupport extends JdbcDaoSupport{
	
	public void insertStudent(Student student) {
		
			String sql= "INSERT INTO student VALUES ("
					 + student.getId() + ",'" + student.getName() + "','" + student.getLocation() + "')";
			this.getJdbcTemplate().execute(sql);
			
			System.out.println("data inserted !!");
			
	}
	public void delelteStudent() {
		
		String sql= "DELETE FROM STUDENT";
		this.getJdbcTemplate().execute(sql);
		
		System.out.println("data deleted !!");
		
	}
	public int count() {
		String query = "Select count(*) from student";
		return this.getJdbcTemplate().queryForObject(query, Integer.class);
	}
	
	public Student getStudentById(int id) {
		String query = "select * from student where id = ?";
		return this.getJdbcTemplate().queryForObject(query, new Object[] {id}, new StudentMapper());
	}
	
	public List<Student> getAllStudent() {
		String query = "SELECT * FROM STUDENT";
		return this.getJdbcTemplate().query(query, new StudentMapper());
	}
	
	
	private static final class StudentMapper implements RowMapper<Student> {
		
		public Student mapRow(ResultSet rs, int arg1) throws SQLException{
			return new Student(rs.getInt("id"),rs.getString("name"),rs.getString("location"));
		}
	}
	
}

