package fa.edu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fa.edu.model.Student;

@Component
public class StudentJdbcDao {

	private String url="jdbc:mysql://localhost:3306/studentmanager?useUnicode=yes&characterEncoding=UTF-8&useSSL=false";
	private String username="root";
	private String password="vuong";
	
	private Connection conn=null;
	private Statement statement = null;
	
	
	
	public void insertStudent(Student student) {
		try {
			createConnection();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			statement = conn.createStatement();
			String sql= "INSERT INTO student VALUES ("
					 + student.getId() + ",'" + student.getName() + "','" + student.getLocation() + "')";
			statement.execute(sql);
			System.out.println("da insert data");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("loi insert data");
		}
	}
	
	public List<Student> getAllStudent() {
		List<Student> allStudent = new ArrayList<Student>();
		try {
			createConnection();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			statement = conn.createStatement();
			
			ResultSet rs = statement.executeQuery("select * from student");
			
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String location = rs.getString(3);
				allStudent.add(new Student(id,name,location));
			}
			rs.close();
			statement.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("error get all student ");
		}
		return allStudent;
	}
	
	private void createConnection() throws SQLException {
		if(conn==null) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				
				conn = DriverManager.getConnection(url, username, password);
				System.out.println("connect database !!");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {System.out.println("database da connect !!");}
	}
	
	private void shutdownConnection() {
		
				try {
					if(statement != null) {
					statement.close();
					}
					if(conn != null) {
						DriverManager.getConnection(url + ";shutdown=true", username, password);
						conn.close();
					}
				} catch (SQLException e) {
					
				}
			
		
	}
	
	
}
