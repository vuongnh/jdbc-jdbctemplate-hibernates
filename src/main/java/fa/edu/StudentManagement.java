package fa.edu;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fa.edu.dao.StudentHibernateDao;
import fa.edu.model.Student;

public class StudentManagement {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		StudentHibernateDao dao = context.getBean("studentHibernateDao", StudentHibernateDao.class);

//		dao.insertStudent(new Student(6,"vuong1","bac ninh 2"));
//		System.out.println(dao.getAllStudent());
//		System.out.println(dao.count());
//		System.out.println(dao.getStudentById(2));
		
		dao.save(new Student("Vuong1","BacNinh"));
		
	}

}
